from django.urls import path

from . import views

app_name = 'quiz'
urlpatterns = [
    path('', views.QuizView.as_view(), name='index'),
    path('register/', views.UserFromView.as_view(), name='register'),
    path('<int:pk>', views.QuizQuestionsView.as_view(), name='quiz-take'),
    
    path('quiz/yourresult/<int:pk>', views.show_taken_quiz, name='taken-quiz'),
    
    # /quiz/add
    path('quiz/create', views.create_quiz, name='quiz-create'),
    path('quiz/save', views.save_quiz, name='save'),
    path('quiz/results', views.UserResultsView.as_view()),
    path('quiz/allresults', views.all_user_results),

]
